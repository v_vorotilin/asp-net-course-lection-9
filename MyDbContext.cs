using Lection9.Controllers;
using Lection9.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lection9 {
    public class MyDbContext : DbContext {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options) {
        }
        
        public DbSet<Animal> Animals { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);
        }
    }
}