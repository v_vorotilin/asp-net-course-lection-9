using Lection9.Entities;

namespace Lection9.Controllers {
    public class Owner {
        public int OwnerId { get; set; }
        public string Name { get; set; }
    }
}