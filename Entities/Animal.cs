using System.ComponentModel.DataAnnotations;
using Lection9.Controllers;

namespace Lection9.Entities {
    public class Animal {
        public int AnimalId { get; set; }
        
        [Required, MaxLength(50)]
        public string Name { get; set; }
        
        public double Height { get; set; }
        
        public int OwnerId { get; set; }
        
        public Owner Owner { get; set; }
    }
}