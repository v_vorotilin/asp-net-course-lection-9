﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Lection9.Migrations
{
    public partial class AddedHeightColumnToAnimalsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Height",
                table: "Animals",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Height",
                table: "Animals");
        }
    }
}
