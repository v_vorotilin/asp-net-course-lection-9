using System.Collections.Generic;
using System.Linq;
using Lection9.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Lection9.Controllers {
    
    [ApiController]
    [Route("[controller]/[action]")]
    public class AnimalController : ControllerBase {

        private readonly MyDbContext _context;
        
        public AnimalController(MyDbContext context) {
            _context = context;
        }
        
        [HttpGet]
        public IActionResult Add([FromQuery] Animal animal) {
            _context.Add(animal);
            _context.SaveChanges();

            return new JsonResult(animal);
        }

        [HttpGet("{height}")]
        public ActionResult<ICollection<Animal>> GetHigherThan([FromRoute] double height) {
            return _context.Animals
                           .Where(a => a.Height > height)
                           .ToList();
        }


        [HttpGet("{animalId}")]
        public ActionResult<Animal> AddOwner([FromRoute] int animalId,
                                             [FromQuery] Owner owner) {
            _context.Add(owner);

            _context.SaveChanges();
            
            var animal = _context.Animals
                                 .Include(a => a.Owner)
                                 .FirstOrDefault(a => a.AnimalId == animalId);

            if (animal == null)
                return NotFound();
            
            animal.Owner = owner;

            _context.SaveChanges();

            return animal;
        }
    }
}